#### This task developed based on laravel 5.4 

### Description
the home page contains a form which has search criteria and send this criteria to the controller through ajax request then receive the data and show it on the browser
i developed 2 ways of testing the noraml test cases (phpunit) and behvaiour driven test cases

#### Deployment Steps:
1- run composer update from the terminal - and make sure to be on the application root.
2- run ( php artisan serve ) from the terminal - and make sure to be on the application root.
3- open http://localhost:8000 in your browser to browse the application.



#### TEST CASES
1- to run test cases (Behaviour Driven) you can run (php artisan dusk) from the terminal  - and make sure to be on the application root.
2- to run test cases (phpunit) you can run ( php vendor/phpunit/phpunit/phpunit ) from the command line.