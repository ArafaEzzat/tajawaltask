<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Http\Controllers\SearchController;

class SearchTest extends TestCase {


    
    
    public function testGetAPIData() {
        //$this->assertTrue(TRUE);
        
        $newInstance = new SearchController();
        $result = $newInstance->getAPIData();
        $count = count($result['hotels']);
        $this->assertArrayHasKey('hotels', $result);
        $this->assertGreaterThan(0, $count);
    }

    public function testSearchtWithCriteria() {
        $newInstance = new SearchController();
        $result = $newInstance->getAPIData();
        
        $nameSearch = $newInstance->searchtWithCriteria($result['hotels'], 'name', 'Media One Hotel');
        $citySearch = $newInstance->searchtWithCriteria($result['hotels'], 'city', 'cairo');
        $priceSearch = $newInstance->searchtWithCriteria($result['hotels'], 'price', 10, 100);
        $dateSearch = $newInstance->searchtWithCriteria($result['hotels'], 'availability', '08/12/2020', '10/26/2020');


        $this->assertGreaterThan(0, count($nameSearch));
        $this->assertGreaterThan(0, count($citySearch));
        $this->assertGreaterThan(0, count($priceSearch));
        $this->assertGreaterThan(0, count($dateSearch));
    }
    
    
    public function testSortData() {

        $newInstance = new SearchController();
        $result = $newInstance->getAPIData();
        
        $nameSortKey = $newInstance->sortData($result['hotels'], 'name');
        $firstElement = $nameSortKey[0]['name'];
        $lastElement = end($nameSortKey);


        $this->assertGreaterThan(0, count($nameSortKey));
        $this->assertEquals('Rotana Hotel', $firstElement);
        $this->assertEquals('Concorde Hotel', $lastElement['name']);
    }    

}
