<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SearchTest extends DuskTestCase
{
    /**
     * Test Home Page function and make sure that it is working will.
     *
     * @return void
     */
    public function testHomePage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Tajawal Task');
        });
    }
    
    
    /**
     * Test Search by Destination and make sure that it is working will.
     *
     * @return void
     */	
    public function testSearchByDestination(){
        $this->browse(function (Browser $browser) {
            $browser->select('search_type', '2')
                    ->pause(500)
                    ->value('#searchVale1', 'cairo')
                    ->click('#search')
                    ->pause(5000)
                    ->assertSee('Rotana Hotel');
        });
    }
    
    
    /**
     * Test Search by Hotel Name and make sure that it is working will.
     *
     * @return void
     */		
    public function testSearchByHotelName(){
        $this->browse(function (Browser $browser) {
            $browser->select('search_type', '1')
                    ->pause(500)
                    ->value('#searchVale1', 'Novotel Hotel')
                    ->click('#search')
                    ->pause(5000)
                    ->assertSee('Novotel Hotel')
                    ->assertSee('111');
        });        
           
    }
    
    
    /**
     * Test Search by Price and make sure that it is working will.
     *
     * @return void
     */		
    public function testSearchByPrice(){
        $this->browse(function (Browser $browser) {
            $browser->select('search_type', '3')
                    ->pause(200)
                    ->value('#searchVale1', '20')
                    ->value('#searchVale2', '500')
                    ->click('#search')
                    ->pause(7000)
                    ->assertSeeIn('tbody tr:first-child', 'Media One Hotel')
                    ->assertSeeIn('tbody tr:last-child', 'Concorde Hotel');
        });        
           
    }   
    
    /**
     * Test Search by Price with sort by price descending and make sure that it is working will.
     *
     * @return void
     */		
    public function testSearchByPriceWithSort(){
        $this->browse(function (Browser $browser) {
            $browser->select('search_type', '3')
                    ->pause(500)
                    ->value('#searchVale1', '20')
                    ->value('#searchVale2', '500')
                    ->select('#sort_type', 'price')
                    ->click('#search')
                    ->pause(5000)
                    ->assertSeeIn('tbody tr:first-child', '111')
                    ->assertSeeIn('tbody tr:last-child', '79.4');
                    
        });        
           
    }     
    
    /**
     * Test Search by availability and make sure that it is working will.
     *
     * @return void
     */			
    public function testSearchByAvailability(){
        $this->browse(function (Browser $browser) {
            $browser->select('search_type', '4')
                    ->pause(500)
                    ->value('#searchVale1', '12/01/2020')
                    ->value('#searchVale2', '12/31/2020')
					->select('#sort_type', '0')
                    ->click('#search')
                    ->pause(5000)
                    ->assertSeeIn('tbody tr:first-child', 'From')
                    ->assertSeeIn('tbody tr:last-child', 'To');
                    
        });     
}

    /**
     * Test Search by availability with sort by hotel name descending and make sure that it is working will.
     *
     * @return void
     */		
    public function testSearchByAvailabilityWithSort(){
        $this->browse(function (Browser $browser) {
            $browser->select('search_type', '4')
                    ->pause(500)
                    ->value('#searchVale1', '12/01/2020')
                    ->value('#searchVale2', '12/31/2020')
                    ->select('#sort_type', 'name')
                    ->click('#search')
                    ->pause(5000)
                    ->assertSeeIn('tbody tr:first-child', 'Rotana Hotel')
                    ->assertSeeIn('tbody tr:last-child', 'Concorde Hotel');
                    
        });     
}

}