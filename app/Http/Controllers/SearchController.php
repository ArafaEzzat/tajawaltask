<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use GuzzleHttp\Client;

class SearchController extends Controller {


    /**
     * this function created to handle search request logic
     * returns josn array contains hotels data
     *
     * 
     * @return array()
     */
    public function searchRequest(Request $request) {

        $this->validate($request, [
            'searchVale1' => 'required',
            'selected_option' => 'required',
            'sort_type' => 'required',
        ]);

        if ($request->ajax()) {
            
            $selected_option = $request->selected_option; 
            $searchVale1 = $request->searchVale1; 
            $searchVale2 = $request->searchVale2; 
            $sort_type = $request->sort_type;
            if($searchVale2 == null){
				$searchVale2='';
			}
            $criteria = '';
            if ($selected_option == 1) {
                $criteria = 'name';
            } elseif ($selected_option == 2) {
                $criteria = 'city';
            } elseif ($selected_option == 3) {
                $criteria = 'price';
            } elseif ($selected_option == 4) {
                $criteria = 'availability';
            }

            
            $arrayOfData = $this->getAPIData();
            $data = $this->searchtWithCriteria($arrayOfData['hotels'], $criteria, $searchVale1, $searchVale2);
            
            if($sort_type !='0'){
                $data = $this->sortData($data, $sort_type);
            }
            
            //return view('searchss', compact('data'));
             return json_encode($data);
        }
    }

    /**
     * this function created to use GuzzleHttp to consume the API
     * returns array contains hotels data
     *
     * 
     * @return array()
     */
    public function getAPIData() {

        $client = new Client(['base_uri' => env('API_URL')]);
        $response = $client->get('');
        $data = $response->getBody();
        $data = json_decode($data, true);
        return $data;
    }

    /**
     * this function created to search in API Data with specific criteria
     * returns array contains filtered data
     *
     * @param Array $data (the data which consumed from the API)
     * @param string $key (the key which will be used in the search)
     * @param mixed $value (can be integer or string according to the key)
     * @param mixed $value2 (can be integer or string according to the key and it has default value)
     * @return array()
     */
    public function searchtWithCriteria(array $data, string $key, string $value, string $value2 = '') :array {

        $return = array();
        foreach ($data as $object) {

            if ($key == 'price') {

                if (isset($object[$key]) && $object[$key] >= $value && $object[$key] <= $value2) {
                    $return[] = $object;
                }
            } elseif ($key == 'availability') {

                foreach ($object[$key] as $availObject) {
                    //print_r($availObject);

                    $objectKey1 = date('Y-m-d', strtotime($availObject['from']));
                    $objectKey2 = date('Y-m-d', strtotime($availObject['to']));
                    //echo $objectKey1;
                    $value = date('Y-m-d', strtotime($value));
                    $value2 = date('Y-m-d', strtotime($value2));

                    if (isset($availObject['from']) && $objectKey1 >= $value && $objectKey2 <= $value2) {
                        $return[$object['name']] = $object;
                    }
                }
            } else {

                if (isset($object[$key]) && $object[$key] == $value) {
                    $return[] = $object;
                }
            }
        }
        return $return;
    }
    
    
    /**
     * this function created to sort the data by specific key
     * @param Array $data (the data which needed to be sorted)
     * @param string $sortKey (the key which will be used in the sorting)
     * @return array()
     */       
    function sortData( array $data, string $sortKey) :array {

        usort($data, function($a, $b) use ($sortKey) {
            return $a[$sortKey] > $b[$sortKey] ? -1 : 1;
        });
        return $data;
    }    

}
