<html>
    <head>
        <meta charset="UTF-8">
        <title>Tajawal - Task</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600,700' rel='stylesheet' type='text/css'>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">        
    </head>
    <body>
    <center>Tajawal Task</center>
    <br/>
    <div class="row">
        <div class="col-lg-6">
            <select  id='search_type' name="search_type" class="form-control select2me search_type">
                <option readonly selected value="0">*** Search Type***</option>
                <option value="1">Hotel Name</option>
                <option value="2">Destination</option>
                <option value="3">Price range</option>
                <option value="4">Date range</option>

            </select>
        </div>
        <div class="col-lg-7">
            <label class="control-label col-md-3" id="search_label"></label>										
            <div class="col-md-4" id="searchField">
            </div>
        </div>	

        <div class="col-lg-6">
            <select  id='sort_type' class="form-control select2me sort_type">
                <option readonly selected value="0">*** Sort Type***</option>
                <option value="name">Hotel Name</option>
                <option value="price">Price</option>


            </select>
        </div>

        <div class="input-group">
<!--                    <input type="text" class="form-control" id="searchData" placeholder="Search for...">-->
            <span class="input-group-btn">
                <button class="btn btn-default" type="button" id="search" name="search">Go!</button>
            </span>
        </div>
    </div>
</div>


<div  id="resultDiv" hidden="hidden">
    <table class="table table-hover">
        <thead>
        <th>Hotel Name</th>
        <th>Price</th>
        <th>City</th>
        <th>Available Dates</th>
        </thead>
        <tbody id="searchResult"></tbody>
    </table>

</div>
<div id="searchResult2"></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src='https://s3-us-west-2.amazonaws.com/s.cdpn.io/87118/jQuery.succinct.min.js'></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
crossorigin="anonymous"></script>
<script src="{{ URL::asset('js/index.js') }}"></script>


<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>    

<script>
    $(document).ready(function () {



        $("#search_type").change(function () {
            $("#searchField").empty();
            $("#searchResult").empty();
            var selected_option = $("#search_type option:selected").val();
            if (selected_option == 1 || selected_option == 2) {
                $("#searchField").empty();
                $("#search_label").empty();
                $('<input type="text" id="searchVale1" name="searchVale1" class="form-control searchVale1" placeholder="Enter Search Value"/>').appendTo($("#searchField"));
                $('<span>Search Value</span>').appendTo($("#search_label"));

            } else if (selected_option == 3) {
                $("#searchField").empty();
                $("#search_label").empty();
                $('<input type="text" id="searchVale1" name="searchVale1" class="form-control searchVale1" placeholder="From"/>').appendTo($("#searchField"));
                $('<input type="text" id="searchVale2" name="searchVale2" class="form-control searchVale2" placeholder="To"/>').appendTo($("#searchField"));

            } else if (selected_option == 4) {
                $("#searchField").empty();
                $("#search_label").empty();
                $('<input type="text" id="searchVale1" name="searchVale1" class="form-control searchVale1 datepicker" placeholder="Enter From Date"/>').appendTo($("#searchField"));
                $('<input type="text" id="searchVale2" name="searchVale2" class="form-control searchVale2 datepicker" placeholder="Enter To Date"/>').appendTo($("#searchField"));
                $(".datepicker").datepicker();
            } else {
                $("#searchField").empty();
                $("#search_label").empty();
            }

        });









        $("#searchData").keyup(function (event) {
            if (event.keyCode == 13) {
                $("#search").click();
            }
        });
        $("#search").click(function () {
            $("#searchResult").empty();
            var selected_option = $("#search_type option:selected").val();
            if (selected_option == '0') {
                alert("Please Select Search Type");
            } else {
                var searchVale1 = $("#searchVale1").val();
                var searchVale2 = $("#searchVale2").val();

                var sort_type = $("#sort_type option:selected").val();
                if (searchVale1.trim() == '') {
                    alert("Please Enter Search Value");
                } else {

                    $('<img src="{{ URL::asset("img/preloader.gif") }}"/>').appendTo($("#searchResult2"));
                    $.post("/SearchResult",
                            {
                                _token: '{!! csrf_token() !!}',
                                searchVale1: searchVale1,
                                searchVale2: searchVale2,
                                selected_option: selected_option,
                                sort_type: sort_type
                            },
                            function (data, status) {
                                //$("#searchResult").html(data);
                                var htmlEle = '';

                                $.each($.parseJSON(data), function (key, value) {

                                    htmlEle += '<tr>';
                                    htmlEle += '<td>' + value.name + '</td>';
                                    htmlEle += '<td>' + value.price + '</td>';
                                    htmlEle += '<td>' + value.city + '</td>';

                                    var availability = value.availability;
                                    htmlEle += '<td>';
                                    $.each(availability, function (key, value) {
                                        htmlEle += "From: " + value.from;
                                        htmlEle += "  To: " + value.to;
                                        htmlEle += "<br/>";
                                    })

                                    htmlEle += '</td>';
                                    console.log(value.availability);
                                    htmlEle += '<td></td>';
                                    htmlEle += '</tr>';
                                });
                                $("#searchResult").html(htmlEle);
                                $("#resultDiv").removeAttr('hidden');
                                $("#searchResult2").empty();
                            });
                }

            }
        }
        );

    })
</script>
</body>
</html>

